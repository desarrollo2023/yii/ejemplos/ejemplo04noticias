<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $foto
 * @property string|null $fechaNacimiento
 * @property string|null $correo
 *
 * @property Noticia[] $noticias
 */
class Autor extends \yii\db\ActiveRecord
{
    public $archivo; // esta es la foto subida
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['fechaNacimiento'], 'safe'],
            [['nombre', 'correo'], 'string', 'max' => 100],
            [['id'], 'unique'],
            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas una imagen
                'extensions' => 'png' // extensiones permitidas
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'correo' => 'Correo',
        ];
    }
    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/autores/' . $this->id . $this->archivo->name);
        return true;
    }

    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        // si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }

        return true;
    }


    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        // compruebo si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->id . $this->archivo->name;
        }

        return true;
    }

    /**
     * metodo que se ejecuta despues de guardar el registro 
     * en la bbdd
     * 
     * @param mixed $insert este argumento es true si estas insertando un registro y false si es una actualizacion
     * @param array $atributosAnteriores tengo todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // pregunto si es una actualizacion
        if (!$insert) {
            // pregunto si he seleccionado un archivo en el formulario
            if (isset($this->archivo)) {
                // compruebo si existia foto anteriormente en la noticia
                if (isset($atributosAnteriores["foto"]) && $atributosAnteriores["foto"] != "") {
                    // elimino la imagen vieja del servidor
                    unlink('imgs/autores/' . $atributosAnteriores["foto"]);
                }
            }
        }
    }

    /**
     * Gets query for [[Noticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticia::class, ['autor' => 'id']);
    }

    public function afterDelete()
    {
        // elimino la imagen de la noticia
        // cuando borro la noticia
        if (isset($this->foto) && file_exists(Yii::getAlias('webroot') . '/imgs/autores/' . $this->foto)) {
            unlink('imgs/autores/' . $this->foto);
        }
        return true;
    }
}
