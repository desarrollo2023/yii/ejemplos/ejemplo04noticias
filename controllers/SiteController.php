<?php

namespace app\controllers;

use app\models\Autor;
use app\models\Noticia;
use app\models\Seccion;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // necesito recuperar todas las
        // noticias de la base de datos
        $consulta = Noticia::find(); // consulta sin ejecutar (activeQuery)
        // $datos=$consulta->all();  // array modelos de noticias
        // $datos1=$consulta->asArray()->all(); // array de noticias

        $datos = $consulta->where(["portada" => 1])->all(); //selecciona solo las noticias que estan en portada

        return $this->render('index', [
            "datos" => $datos,
            "titulo" => "Noticias de portada"
        ]);
    }
    public function actionViewnoticia($idNoticia)
    {
        $consulta = Noticia::find()->where([
            "idNoticia" => $idNoticia
        ]); // select * from noticia where idNoticia=1

        $dato = $consulta->one();

        return $this->render("verNoticia", [
            "dato" => $dato
        ]);
    }
    public function actionSecciones()
    {
        $secciones = Seccion::find()->all(); //select * from seccion
        return $this->render("secciones", ["secciones" => $secciones]);
    }
    public function actionSeccion($id)
    {
        $noticias = Noticia::find()->where(["seccion" => $id])->all();
        //saco todos los datos de la seccion seleccionada
        $seccion = Seccion::find()->where(['id' => $id])->one();
        //$seccion = Seccion::findOne($id);
        return $this->render("index", ["datos" => $noticias, "titulo" => "Noticias de la Seccion " . $seccion->nombre]);
    }


    public function actionAutores()
    {
        $autores = Autor::find()->all();
        return $this->render("autores", ["autores" => $autores]);
    }
    public function actionAutor($id)
    {
        $noticias = Noticia::find()->where(["autor" => $id])->all();
        //saco todos los datos de la seccion seleccionada
        $autor = Autor::find()->where(['id' => $id])->one();
        return $this->render("index", ["datos" => $noticias, "titulo" => "Noticias de " . $autor->nombre]);
    }
}
